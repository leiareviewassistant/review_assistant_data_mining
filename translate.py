#!/usr/bin/env python

# Copyright 2016 Google, Inc
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Analyzes text using the Google Cloud Natural Language API."""

from googleapiclient import discovery


def get_service():
    return discovery.build('translate', 'v2', developerKey="AIzaSyDulJx_LKblkvtUwKebkOpT2FeTDgT4rWc")


def translate(text, source='ro', target='en'):
    service = get_service()

    request = service.translations().list(
      source=source,
      target=target,
      q=text
    )
    response = request.execute()

    return response

import mysql.connector
from sys import argv
import os

cnx = mysql.connector.connect(host='127.0.0.1', database='reviews', user='reviews', password='reviews')
cursor = cnx.cursor()
script, directory, search_terms = argv

destination_file = search_terms.split()[0]

directory = os.path.abspath(directory)
if not os.path.exists(directory):
    os.makedirs(directory)
destination_file = os.path.join(directory, destination_file)

query = (" SELECT content"
         "   FROM eos_feedback ")

params = []
is_first = 1
for term in search_terms.split():
    if is_first == 1:
        query = query + " WHERE "
        is_first = 0
    else:
        query = query + " OR "
    query = query + " content LIKE %s "
    params.append("%" + term + "%")

cursor.execute(query, params)
has_rows = 0
target = open(destination_file, 'w+')

for content in cursor:
  text = content[0].encode('utf-8')
  text = text.replace("<br />", "\n")
  text = text.replace("\r\n", "\n")
  target.write(text)
  target.write("\n")
  has_rows = has_rows + 1

target.close()

if has_rows == 0:
    os.remove(destination_file)
    print ('no results, so no file')

cursor.close()
cnx.close()
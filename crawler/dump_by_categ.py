import mysql.connector
from sys import argv
import os

cnx = mysql.connector.connect(host='127.0.0.1', database='reviews', user='reviews', password='reviews')
cursor = cnx.cursor()
script, directory, category_id = argv

print ('building category {}'.format(category_id))

directory = os.path.abspath(directory)
if not os.path.exists(directory):
    os.makedirs(directory)
destination_file = os.path.join(directory, category_id)

query = (" SELECT "
        "  categories.categories_name,"
        "  characteristics_translation.name,"
        "  characteristics_translation.description"
        " FROM products "
        "  INNER JOIN categories ON categories.categories_id = products.category_id "
        "  INNER JOIN product_characteristics ON products.id = product_characteristics.product_id"
        "  INNER JOIN characteristic_groups_translation ON product_characteristics.characteristic_group_id = characteristic_groups_translation.characteristic_group_id"
        "  INNER JOIN characteristics_translation ON product_characteristics.characteristic_id = characteristics_translation.characteristic_id"
        " WHERE 1=1"
        "  AND product_characteristics.is_visible = 1"
        "      AND products.category_id = %s "
        "      AND products.is_active = 1"
        " GROUP BY products.category_id,"
        "  characteristics_translation.name,"
        "  characteristics_translation.description")

cursor.execute(query, [category_id])
has_rows = 0
target = open(destination_file, 'w+')

for (category_name, characteristic_name, characteristic_description) in cursor:
  target.write("{} {}".format(characteristic_name.encode('utf-8'), characteristic_description.encode('utf-8')))
  target.write("\n")
  has_rows = has_rows + 1

target.close()

if has_rows == 0:
    os.remove(destination_file)

cursor.close()
cnx.close()
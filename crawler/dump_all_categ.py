import mysql.connector
from sys import argv
from subprocess import call
import os

script, directory = argv
called_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'dump_by_categ.py')

cnx = mysql.connector.connect(host='127.0.0.1', database='reviews', user='reviews', password='reviews')
cursor = cnx.cursor()

query = (" SELECT DISTINCT products.category_id"
         "   FROM products "
         "   WHERE products.is_active = 1")

cursor.execute(query)

for (category_id) in cursor:
    call(['python', called_file, directory, str(category_id[0])])

cursor.close()
cnx.close()
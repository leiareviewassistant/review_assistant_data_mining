from flask import Flask
from flask import request
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/translate/<labelname>')
def show_results(labelname=None):
    results1 = request.args.get('results1', '0-100')

if __name__ == '__main__':
    app.run()
